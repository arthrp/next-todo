import { Button } from "@material-tailwind/react";
import { Checkbox } from "@material-tailwind/react";
import { Input } from "@material-tailwind/react";
import { useState } from "react";
import { v4 as uuidv4 } from 'uuid';
import  css  from "@/components/todoList/todoList.module.css";

interface ITodoItem{
    id: string,
    text: string,
    isDone: boolean
}

export default function TodoList(){
    const [todos, setTodos] = useState<ITodoItem[]>([
        {id: uuidv4(), text: "My first item", isDone: true}
    ]);
    const [newTodoText, setNewTodoText] = useState<string>("");

    function toggleTodoCompleted(index: number){
        const tmpTodos = [...todos];
        tmpTodos[index].isDone = !tmpTodos[index].isDone;
        setTodos(tmpTodos);
    }

    function addTodo(text: string){
        const tmpTodos = [...todos];
        tmpTodos.push({id: uuidv4(), text: text, isDone: false});
        setTodos(tmpTodos);
    }

    function deleteTodo(index: number){
        const tmpTodos = [...todos];
        tmpTodos.splice(index, 1);
        setTodos(tmpTodos);
    }

    return (
        <>
        <h1 className="text-3xl font-bold underline">TODOs</h1>
        <div className="flex flex-col">
            {todos.map((t, i) => (
                <div className={`max-w-md flex flex-row space-x-4 ${css.todoItem}`} key={i}>
                    <Checkbox checked={t.isDone} label={t.text} onChange={() => toggleTodoCompleted(i)} ripple={false} />
                    <Button color="red" size="sm" onClick={() => deleteTodo(i)}>Delete</Button>
                </div>
            ))}
            <div className="max-w-md flex flex-row space-x-4">
                <Input label="New todo" value={newTodoText} onChange={(e) => setNewTodoText(e.target.value)} />
                <Button onClick={() => addTodo(newTodoText)}>Add</Button>
            </div>
        </div>
        </>
    );
}