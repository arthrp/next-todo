import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import  TodoList from '@/components/todoList/todoList';
import styles from '@/styles/Home.module.css'
// @material-tailwind/react
import { ThemeProvider } from "@material-tailwind/react";

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
      <Head>
        <title>TODO app</title>
        <meta name="description" content="Todo app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <ThemeProvider>
        <div id="container">
          <TodoList />
        </div>
      </ThemeProvider>
    </>
  )
}
