This is a [Next.js](https://nextjs.org/) based TODO app.

## Getting Started

To run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```